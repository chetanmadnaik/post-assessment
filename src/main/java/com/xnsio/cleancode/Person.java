package com.xnsio.cleancode;

import java.time.LocalDateTime;

public class Person {
    private String name;
    private Calender calender;

    public String getName() {
        return name;
    }

    public Person(String name) {
        this.name = name;
        this.calender = new Calender();
    }

    public boolean isAvailable(LocalDateTime localDateTime) {
        return this.calender.isAvailable(localDateTime);
    }

    public void addSchedule(LocalDateTime localDateTime, String reason) {
        this.calender.addSchedule(localDateTime, reason);
    }
}

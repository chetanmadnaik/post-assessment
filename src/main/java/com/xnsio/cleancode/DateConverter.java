package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateConverter {

    public static LocalDateTime getNextHourMark(LocalDateTime now) {
        return now.plusHours(1).withMinute(0).withSecond(0).withNano(0);
    }

    public static LocalDateTime dateFromString(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);
        return dateTime;
    }
}

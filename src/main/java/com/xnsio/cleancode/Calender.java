package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Calender {

    private Map<LocalDateTime, String> schedules = new TreeMap<>();

    public Map<LocalDateTime, String> getSchedules() {
        return schedules;
    }

    public boolean isAvailable(LocalDateTime localDateTime) {
        for (Map.Entry<LocalDateTime, String> schedule : schedules.entrySet()) {
            if (schedule.getKey().equals(localDateTime)) {
                return false;
            }
        }
        return true;
    }

    public void addSchedule(LocalDateTime localDateTime, String reason) throws RuntimeException {
        if (Objects.nonNull(localDateTime) && Objects.nonNull(reason))
            schedules.put(localDateTime, reason);
        else
            throw new RuntimeException("please verify schedule date and reason");
    }
}

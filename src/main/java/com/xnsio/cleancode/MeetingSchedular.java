package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.*;

import static com.xnsio.cleancode.DateConverter.getNextHourMark;

public class MeetingSchedular {

    private Map<String, Person> employees = new HashMap<>();

    public void addEmployee(Person person) {
        employees.put(person.getName(), person);
    }

    public Map<String, Person> getEmployees() {
        return this.employees;
    }

    public LocalDateTime getFirstAvailableMeetingSlot(List<String> meetingCandidates, LocalDateTime untilDate) throws RuntimeException {
        HashSet<Person> personels = getMeetingPersonels(meetingCandidates);
        List<LocalDateTime> hourMarks = getAllHourMarksToDateFromNow(untilDate);
        Collections.sort(hourMarks);
        if (!hourMarks.isEmpty()) {
            for (LocalDateTime hourMark : hourMarks) {
                if (isHourMarkAvailableForAll(hourMark, personels))
                    return hourMark;
            }
        } else {
            throw new RuntimeException("Date given is not Valid. Should be atleast 1 hour later from now");
        }
        return null;
    }

    private boolean isHourMarkAvailableForAll(LocalDateTime hourMark,  HashSet<Person> personels) {
        Iterator<Person> iterator = personels.iterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (!person.isAvailable(hourMark)) {
                return false;
            }
        }
        return true;
    }

    private List<LocalDateTime> getAllHourMarksToDateFromNow(LocalDateTime untillDate) {
        List<LocalDateTime> hourMarks = new LinkedList<>();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nextHour = null;
        do {
            nextHour = getNextHourMark(now);
            hourMarks.add(nextHour);
            now = nextHour;
        } while (nextHour.isBefore(untillDate));

        return hourMarks;
    }


    private HashSet<Person> getMeetingPersonels(List<String> meetingCandidates) {
        HashSet<Person> meetingPersonal = new HashSet<>();
        meetingCandidates.stream().forEach(meetingCandidate -> {
            if (employees.containsKey(meetingCandidate))
                meetingPersonal.add(employees.get(meetingCandidate));
            else
                throw new RuntimeException("meeting candidate named " + meetingCandidate + " is not registered.");
        });

        return meetingPersonal;
    }

    public LocalDateTime bookFirstAvailableSlot(List<String> meetingCandidates, LocalDateTime untilDate, String reason) throws RuntimeException {
        LocalDateTime availableSlot = getFirstAvailableMeetingSlot(meetingCandidates, untilDate);
        if (availableSlot.equals(untilDate)) {
            throw new RuntimeException("No slot is available til given Date for given people.");
        }
        bookSlotForCandidates(meetingCandidates, availableSlot, reason);
        return availableSlot;
    }

    private void bookSlotForCandidates(List<String> meetingCandidates, LocalDateTime availableSlot,String reason) {
        HashSet<Person> personels = getMeetingPersonels(meetingCandidates);
        personels.stream().forEach(person -> person.addSchedule(availableSlot, reason));
    }
}

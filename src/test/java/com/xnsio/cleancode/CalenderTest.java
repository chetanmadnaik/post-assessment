package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDateTime;

import static com.xnsio.cleancode.DateConverter.dateFromString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CalenderTest {
    public Calender calender = new Calender();

    @Test
    public void addSchedule() {
        LocalDateTime localDateTime = dateFromString("1986-04-08 12:00");
        calender.addSchedule(localDateTime, "test");
        assertFalse(calender.getSchedules().isEmpty());
    }

    @Test
    public void isAvailableShouldReturnFalseWhenAlreadyScheduled() {
        LocalDateTime localDateTime = dateFromString("1986-04-08 12:00");
        calender.addSchedule(localDateTime, "test");
        assertFalse(calender.isAvailable(localDateTime));
    }

    @Test
    public void isAvailableShouldReturnTrueWhenNoSchedulesPresent() {
        LocalDateTime localDateTime = dateFromString("1986-04-08 12:00");
        assertTrue(calender.isAvailable(localDateTime));
    }
}

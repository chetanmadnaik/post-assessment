package com.xnsio.cleancode;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDateTime;
import java.util.Arrays;

import static com.xnsio.cleancode.DateConverter.getNextHourMark;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MeetingSchedularTest {

    public MeetingSchedular meetingSchedular = new MeetingSchedular();
    LocalDateTime time1 = getNextHourMark(LocalDateTime.now());
    LocalDateTime time2 = getNextHourMark(time1);
    LocalDateTime time3 = getNextHourMark(time2);
    LocalDateTime time4 = getNextHourMark(time3);
    LocalDateTime time5 = getNextHourMark(time4);
    LocalDateTime time6 = getNextHourMark(time5);
    LocalDateTime time7 = getNextHourMark(time6);
    LocalDateTime time8 = getNextHourMark(time7);
    LocalDateTime time9 = getNextHourMark(time8);

    Person chetan = new Person("Chetan");
    Person kartik = new Person("Kartik");
    Person ram = new Person("Ram");

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void addEmployee() {
        meetingSchedular.addEmployee(chetan);
        assertFalse(meetingSchedular.getEmployees().isEmpty());
    }

    @Test
    public void getFirstAvailableSlotWhenNoScheduledShouldReturnNextHour() {
        meetingSchedular.addEmployee(chetan);
        meetingSchedular.addEmployee(kartik);
        meetingSchedular.addEmployee(ram);
        assertEquals(time1, meetingSchedular.getFirstAvailableMeetingSlot(Arrays.asList("Chetan", "Kartik", "Ram"), getNextHourMark(time9)));
    }


    @Test
    public void getFirstAvailableSlotWhenScheduled() {
        chetan.addSchedule(time1, "scrum");
        chetan.addSchedule(time2, "QA");
        chetan.addSchedule(time4, "Dev Meeting");
        chetan.addSchedule(time6, "Dev Meeting");
        chetan.addSchedule(time7, "Technology");
        chetan.addSchedule(time8, "Dev Meeting");

        kartik.addSchedule(time1, "scrum");

        ram.addSchedule(time2, "Dev Meeting");
        ram.addSchedule(time3, "Dev Meeting");
        ram.addSchedule(time7, "Dev Meeting");
        ram.addSchedule(time9, "Dev Meeting");

        meetingSchedular.addEmployee(chetan);
        meetingSchedular.addEmployee(kartik);
        meetingSchedular.addEmployee(ram);


        assertEquals(time5, meetingSchedular.getFirstAvailableMeetingSlot(Arrays.asList("Chetan", "Kartik", "Ram"), getNextHourMark(time9)));
        assertEquals(time4, meetingSchedular.getFirstAvailableMeetingSlot(Arrays.asList("Kartik", "Ram"), getNextHourMark(time9)));
        assertEquals(time3, meetingSchedular.getFirstAvailableMeetingSlot(Arrays.asList("Chetan", "Kartik"), getNextHourMark(time9)));
        assertEquals(time5, meetingSchedular.getFirstAvailableMeetingSlot(Arrays.asList("Chetan", "Ram"), getNextHourMark(time9)));
    }


    @Test
    public void bookFirstAvailableSlotWhenScheduled() {
        chetan.addSchedule(time1, "scrum");
        chetan.addSchedule(time2, "QA");
        chetan.addSchedule(time4, "Dev Meeting");
        chetan.addSchedule(time6, "Dev Meeting");
        chetan.addSchedule(time7, "Technology");
        chetan.addSchedule(time8, "Dev Meeting");

        kartik.addSchedule(time1, "scrum");

        ram.addSchedule(time2, "Dev Meeting");
        ram.addSchedule(time3, "Dev Meeting");
        ram.addSchedule(time7, "Dev Meeting");
        ram.addSchedule(time9, "Dev Meeting");

        meetingSchedular.addEmployee(chetan);
        meetingSchedular.addEmployee(kartik);
        meetingSchedular.addEmployee(ram);


        assertEquals(time5, meetingSchedular.bookFirstAvailableSlot(Arrays.asList("Chetan", "Kartik", "Ram"), getNextHourMark(time9), "Monthly  release Track"));

    }

    @Test
    public void bookFirstAvailableSlotForUnknownPersonShouldThrowException() {

        thrown.expect(RuntimeException.class);
        thrown.expectMessage(containsString("meeting candidate named Sneha is not registered."));

        chetan.addSchedule(time1, "scrum");
        chetan.addSchedule(time2, "QA");
        chetan.addSchedule(time4, "Dev Meeting");
        chetan.addSchedule(time6, "Dev Meeting");
        chetan.addSchedule(time7, "Technology");
        chetan.addSchedule(time8, "Dev Meeting");

        kartik.addSchedule(time1, "scrum");

        ram.addSchedule(time2, "Dev Meeting");
        ram.addSchedule(time3, "Dev Meeting");
        ram.addSchedule(time7, "Dev Meeting");
        ram.addSchedule(time9, "Dev Meeting");

        meetingSchedular.addEmployee(chetan);
        meetingSchedular.addEmployee(kartik);
        meetingSchedular.addEmployee(ram);


        assertEquals(time5, meetingSchedular.bookFirstAvailableSlot(Arrays.asList("Chetan", "Kartik", "Ram", "Sneha"), getNextHourMark(time9), "Monthly  release Track"));

    }

    @Test
    public void bookFirstAvailableSlotWhenScheduledAndSlotNotAvailableUntilGivenDate() {
        thrown.expect(RuntimeException.class);
        thrown.expectMessage(containsString("No slot is available til given Date for given people."));

        chetan.addSchedule(time1, "scrum");
        chetan.addSchedule(time2, "QA");
        chetan.addSchedule(time4, "Dev Meeting");
        chetan.addSchedule(time6, "Dev Meeting");
        chetan.addSchedule(time7, "Technology");
        chetan.addSchedule(time8, "Dev Meeting");

        kartik.addSchedule(time1, "scrum");

        ram.addSchedule(time2, "Dev Meeting");
        ram.addSchedule(time3, "Dev Meeting");
        ram.addSchedule(time5, "Dev Meeting");
        ram.addSchedule(time9, "Dev Meeting");

        meetingSchedular.addEmployee(chetan);
        meetingSchedular.addEmployee(kartik);
        meetingSchedular.addEmployee(ram);


        assertEquals(time5, meetingSchedular.bookFirstAvailableSlot(Arrays.asList("Chetan", "Kartik", "Ram"), getNextHourMark(time9), "Monthly  release Track"));

    }
}

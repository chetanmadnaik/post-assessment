package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDateTime;

import static com.xnsio.cleancode.DateConverter.dateFromString;
import static com.xnsio.cleancode.DateConverter.getNextHourMark;
import static org.junit.Assert.assertEquals;

public class DateConverterTest {

    @Test
    public void getNextHourMarkTest() {
        LocalDateTime date = dateFromString("1986-04-08 12:00");
        LocalDateTime nextHourFromDate = getNextHourMark(date);
        assertEquals(8, nextHourFromDate.getDayOfMonth());
        assertEquals(4, nextHourFromDate.getMonth().getValue());
        assertEquals(1986, nextHourFromDate.getYear());
        assertEquals(13, nextHourFromDate.getHour());
    }

    @Test
    public void dateFromStringTest() {
        LocalDateTime date = dateFromString("1986-04-08 12:00");
        assertEquals(8, date.getDayOfMonth());
        assertEquals(4, date.getMonth().getValue());
        assertEquals(1986, date.getYear());
        assertEquals(12, date.getHour());
    }


}

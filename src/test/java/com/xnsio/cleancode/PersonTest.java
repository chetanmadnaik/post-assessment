package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDateTime;

import static com.xnsio.cleancode.DateConverter.dateFromString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonTest {

    @Test
    public void availabilityOfPersonForExistingScheduleShouldReturnTrue() {
        Person person = new Person("chetan");
        LocalDateTime localDateTime = dateFromString("1986-04-08 12:00");
        assertTrue(person.isAvailable(localDateTime));
    }

    @Test
    public void availabilityOfPersonForExistingScheduleShouldReturnFalse() {
        Person person = new Person("chetan");
        LocalDateTime localDateTime = dateFromString("1986-04-08 12:00");
        person.addSchedule(localDateTime, "Indo-us Security");

        assertFalse(person.isAvailable(localDateTime));
    }



}
